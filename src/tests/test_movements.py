from typing import Callable
from unittest import TestCase

from contract.models import Position, Direction
from service import RoverService

X: int = 3444980
Y: int = 9089087


class TestMovements(TestCase):
    def test_handle_F_headed_north(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.NORTH)
        expected_output: Position = Position(
            x=start_position.x, y=start_position.y + 1, direction=start_position.direction
        )

        output = RoverService._handle_F(start_position)

        assert expected_output == output

    def test_handle_F_headed_south(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.SOUTH)
        expected_output: Position = Position(
            x=start_position.x, y=start_position.y - 1, direction=start_position.direction
        )

        output = RoverService._handle_F(start_position)

        assert expected_output == output

    def test_handle_F_headed_west(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.WEST)
        expected_output: Position = Position(
            x=start_position.x - 1, y=start_position.y, direction=start_position.direction
        )

        output = RoverService._handle_F(start_position)

        assert expected_output == output

    def test_handle_F_headed_east(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.EAST)
        expected_output: Position = Position(
            x=start_position.x + 1, y=start_position.y, direction=start_position.direction
        )

        output = RoverService._handle_F(start_position)

        assert expected_output == output

    def test_handle_B_headed_north(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.NORTH)
        expected_output: Position = Position(
            x=start_position.x, y=start_position.y - 1, direction=start_position.direction
        )

        output = RoverService._handle_B(start_position)

        assert expected_output == output

    def test_handle_B_headed_south(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.SOUTH)
        expected_output: Position = Position(
            x=start_position.x, y=start_position.y + 1, direction=start_position.direction
        )

        output = RoverService._handle_B(start_position)

        assert expected_output == output

    def test_handle_B_headed_west(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.WEST)
        expected_output: Position = Position(
            x=start_position.x + 1, y=start_position.y, direction=start_position.direction
        )

        output = RoverService._handle_B(start_position)

        assert expected_output == output

    def test_handle_B_headed_east(self) -> None:
        start_position: Position = Position(x=X, y=Y, direction=Direction.EAST)
        expected_output: Position = Position(
            x=start_position.x - 1, y=start_position.y, direction=start_position.direction
        )

        output = RoverService._handle_B(start_position)

        assert expected_output == output

    @staticmethod
    def __test_rotation(
        tested_function: Callable[[Position], Position], start_direction: Direction, exptected_direction: Direction
    ) -> None:
        start_position: Position = Position(x=X, y=Y, direction=start_direction)
        expected_output: Position = Position(x=start_position.x, y=start_position.y, direction=exptected_direction)

        output = tested_function(start_position)

        assert expected_output == output
