from unittest import TestCase

from starlette.testclient import TestClient

from api import mars_rover_kata_app
from contract.models import Position, Direction


class TestApi(TestCase):
    def test_valid_command(self) -> None:
        client: TestClient = TestClient(mars_rover_kata_app)

        landing_position: Position = Position(x=0, y=0, direction=Direction.NORTH)

        response = client.post(url=f"/move/FLFFFRFLB", data=landing_position.json(exclude_none=True))

        assert response.status_code == 200
        reported_position: Position = Position.parse_raw(response.content.decode())
        assert reported_position.x == -2
        assert reported_position.y == 2
        assert reported_position.direction == Direction.WEST

    def test_invalid_command(self) -> None:
        client: TestClient = TestClient(mars_rover_kata_app)

        landing_position: Position = Position(x=0, y=0, direction=Direction.NORTH)

        response = client.post(url=f"/move/f3", data=landing_position.json(exclude_none=True))

        assert response.status_code == 422

    def test_missing_starting_position(self) -> None:
        client: TestClient = TestClient(mars_rover_kata_app)

        response = client.post(url=f"/move/FF")

        assert response.status_code == 422
