from typing import Callable
from unittest import TestCase

from contract.models import Position, Direction
from service import RoverService

X: int = 98090
Y: int = 98788


class TestRotations(TestCase):
    @staticmethod
    def __test_rotation(
        tested_function: Callable[[Position], Position], start_direction: Direction, exptected_direction: Direction
    ) -> None:
        start_position: Position = Position(x=X, y=Y, direction=start_direction)
        expected_output: Position = Position(x=start_position.x, y=start_position.y, direction=exptected_direction)

        output = tested_function(start_position)

        assert expected_output == output

    def test_handle_L_headed_north(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_L, start_direction=Direction.NORTH, exptected_direction=Direction.WEST
        )

    def test_handle_L_headed_west(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_L, start_direction=Direction.WEST, exptected_direction=Direction.SOUTH
        )

    def test_handle_L_headed_south(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_L, start_direction=Direction.SOUTH, exptected_direction=Direction.EAST
        )

    def test_handle_L_headed_east(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_L, start_direction=Direction.EAST, exptected_direction=Direction.NORTH
        )

    def test_handle_R_headed_north(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_R, start_direction=Direction.NORTH, exptected_direction=Direction.EAST
        )

    def test_handle_R_headed_west(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_R, start_direction=Direction.WEST, exptected_direction=Direction.NORTH
        )

    def test_handle_R_headed_south(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_R, start_direction=Direction.SOUTH, exptected_direction=Direction.WEST
        )

    def test_handle_R_headed_east(self) -> None:
        self.__test_rotation(
            tested_function=RoverService._handle_R, start_direction=Direction.EAST, exptected_direction=Direction.SOUTH
        )
