from contract.models import Position, RoverCommand, MovementType, Direction


class RoverService:
    @staticmethod
    def handle_command(command: RoverCommand, start_position: Position) -> Position:
        current_position: Position = start_position
        entry: str
        for entry in command.__root__:
            if entry == MovementType.F.value:
                current_position = RoverService._handle_F(current_position)
            elif entry == MovementType.B.value:
                current_position = RoverService._handle_B(current_position)
            elif entry == MovementType.L.value:
                current_position = RoverService._handle_L(current_position)
            elif entry == MovementType.R.value:
                current_position = RoverService._handle_R(current_position)
        return current_position

    @staticmethod
    def _handle_F(input: Position, movement_unit: int = 1) -> Position:
        if input.direction == Direction.EAST:
            return Position(x=input.x + movement_unit, y=input.y, direction=input.direction)
        if input.direction == Direction.WEST:
            return Position(x=input.x - movement_unit, y=input.y, direction=input.direction)
        if input.direction == Direction.NORTH:
            return Position(x=input.x, y=input.y + movement_unit, direction=input.direction)
        if input.direction == Direction.SOUTH:
            return Position(x=input.x, y=input.y - movement_unit, direction=input.direction)

    @staticmethod
    def _handle_B(input: Position) -> Position:
        return RoverService._handle_F(input=input, movement_unit=-1)

    @staticmethod
    def _handle_L(input: Position) -> Position:
        if input.direction == Direction.EAST:
            return Position(x=input.x, y=input.y, direction=Direction.NORTH)
        if input.direction == Direction.NORTH:
            return Position(x=input.x, y=input.y, direction=Direction.WEST)
        if input.direction == Direction.WEST:
            return Position(x=input.x, y=input.y, direction=Direction.SOUTH)
        if input.direction == Direction.SOUTH:
            return Position(x=input.x, y=input.y, direction=Direction.EAST)

    @staticmethod
    def _handle_R(input: Position) -> Position:
        if input.direction == Direction.EAST:
            return Position(x=input.x, y=input.y, direction=Direction.SOUTH)
        if input.direction == Direction.SOUTH:
            return Position(x=input.x, y=input.y, direction=Direction.WEST)
        if input.direction == Direction.WEST:
            return Position(x=input.x, y=input.y, direction=Direction.NORTH)
        if input.direction == Direction.NORTH:
            return Position(x=input.x, y=input.y, direction=Direction.EAST)
