#!/usr/bin/env bash
set -e
PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "${PARENT_PATH}"
pip3 install -r requirements.txt
pip3 install -r ./requirements-test.txt
pip3 install -r ./requirements-dev.txt
rm -rf ./src/contract
rm -rf ./build
mkdir -p ./src/contract
datamodel-codegen --input "${PARENT_PATH}"/contract.yaml --output src/contract/models.py
rm -rf "${PARENT_PATH}"/build

