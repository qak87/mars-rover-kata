## Mars Rover Kata

Version: 1.4, Last updated: 2021-02-02
Implementation is available at [Gitlab](https://gitlab.com/qak87/mars-rover-kata)


# Summary

The repository contains Mars Rover Kata Task implementation as REST application using:

* Python 3.9
* FastApi
* OpenApi 3 specification

together with Gitlab CI toolset to verify code correctness.

# Build process

## Perquisites

The machine that project is built and run on needs to
have [Python 3.9](https://www.python.org/downloads/release/python-390/) installed.

## Build and run instructions:

* Run script **./test.sh** - this will install all the needed libraries and execute all the unit tests to verify proper
  installation
* Run script **./runLocally.sh** - this will launch *uvicorn* server and start serving the application on the local
  machine
* Open [http://localhost:8080/docs](http://localhost:8080/docs) in the browser and interact with application through
  well known Swagger UI

### Build and run in Docker

If you have any problems running or building it on your local machine please use docker with commands:
* `docker build -t qak87/mars-rover-kata .`
* `docker run --name mars-rover-kata -p 80:80 qak87/mars-rover-kata`

and open [http://localhost:80/docs](http://localhost:80/docs) in browser

