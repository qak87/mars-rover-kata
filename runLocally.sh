#!/usr/bin/env bash
set -e
PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo Script location: "${PARENT_PATH}"
cd "${PARENT_PATH}"/src
echo 'Run:  http://localhost:8080/docs'
uvicorn api:mars_rover_kata_app --host localhost --port 8080