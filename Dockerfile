FROM python:3.9-slim
WORKDIR /mars-rover-kata
COPY ./ /mars-rover-kata
RUN ./test.sh
WORKDIR /mars-rover-kata/src
CMD ["uvicorn", "api:mars_rover_kata_app", "--host", "0.0.0.0", "--port", "80"]